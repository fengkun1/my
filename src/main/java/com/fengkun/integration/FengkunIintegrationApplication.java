package com.fengkun.integration;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FengkunIintegrationApplication {

    public static void main(String[] args) {
        SpringApplication.run(FengkunIintegrationApplication.class, args);
    }

}
