package com.fengkun.integration.config;

import com.fengkun.integration.common.trace.TraceUtil;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import java.io.IOException;

@Component
public class MyFilter implements Filter {



    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        System.out.println("init");
    }


    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
      try {
          String traceId = TraceUtil.INSTANCE.getTraceId();

          filterChain.doFilter(servletRequest,servletResponse);
      }finally {
          TraceUtil.INSTANCE.clear();
      }
    }

    @Override
    public void destroy() {

        System.out.println("destroy : "+ TraceUtil.INSTANCE.getTraceId1());

    }
}