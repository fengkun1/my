package com.fengkun.integration.common.util;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.alibaba.excel.exception.ExcelDataConvertException;
import com.google.common.collect.Lists;

import java.util.List;

/**
 * easyexcel导入监听
 *
 * @author dingchunlei
 * @date 2020/08/24
 */
public class OrderExcelDataListener<T> extends AnalysisEventListener<T> {

    public static final int MAX_COUNT_EXCE = 10000;

    List<T> list = Lists.newArrayList();

    /**
     * 导入数量
     */
    int importQuantity = 0;

    /**
     * 这个每一条数据解析都会来调用
     *
     * @param data excel数据
     */
    @Override
    public void invoke(T data, AnalysisContext context) {
        if (list.size() < MAX_COUNT_EXCE + 1) {
            list.add(data);
        }
        ++importQuantity;
    }

    /**
     * 所有数据解析完成了 都会来调用
     *
     * @param context
     */
    @Override
    public void doAfterAllAnalysed(AnalysisContext context) {
        // 这里也要保存数据，确保最后遗留的数据也存储到数据库
    }

    /**
     * 返回excel读取的数据
     *
     * @return excel读取的数据
     */
    public List<T> getData() {
        return list;
    }

    public int getImportQuantity(){
        return importQuantity;
    }

    @Override
    public void onException(Exception exception, AnalysisContext context) {
        // 如果是某一个单元格的转换异常 能获取到具体行号
        // 如果要获取头的信息 配合invokeHeadMap使用
        if (exception instanceof ExcelDataConvertException) {
            ExcelDataConvertException excelDataConvertException = (ExcelDataConvertException)exception;
        }
    }

}
