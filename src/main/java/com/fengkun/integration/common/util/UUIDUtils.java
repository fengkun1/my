package com.fengkun.integration.common.util;

import java.util.UUID;

public class UUIDUtils {

    public UUIDUtils() {
    }

    public static String getUuid() {
        UUID uuid = UUID.randomUUID();
        return uuid.toString().replaceAll("-", "");
    }


}
