package com.fengkun.integration.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;

/**
 * @author fengkun
 */

@AllArgsConstructor
@Getter
public enum ServiceEnum {
    /**
     * 默认值
     */
    ON("200", "zai"),

    /**
     * 骑手管理服务
     */
    ONLIE("300", "在线"),

    /**
     * 骑手API服务
     */
    OFF("400", "离线"),



    ;

    /**
     * 服务上下文
     */
    private final String code;

    /**
     * 服务标识
     */
    private final String msg;


    public static ServiceEnum getEnums(String code) {
        return Arrays.stream(ServiceEnum.values())
                .filter(i -> i.getCode().equals(code))
                .findFirst().orElse(null);

    }


}
