package com.fengkun.integration.common.trace;

import lombok.NonNull;

import java.io.Serializable;

public class TraceData implements Serializable {
    private @NonNull String traceId;
    private String spanId;
    private String parentSpanId;

    public String toString() {
        String traceString = "{traceId='" + this.traceId + '\'';
        traceString = traceString + '}';
        return traceString;
    }

    public void setTraceId(@NonNull String traceId) {
        if (traceId == null) {
            throw new NullPointerException("traceId is marked non-null but is null");
        } else {
            this.traceId = traceId;
        }
    }

    public void setSpanId(String spanId) {
        this.spanId = spanId;
    }

    public void setParentSpanId(String parentSpanId) {
        this.parentSpanId = parentSpanId;
    }

    public @NonNull String getTraceId() {
        return this.traceId;
    }

    public String getSpanId() {
        return this.spanId;
    }

    public String getParentSpanId() {
        return this.parentSpanId;
    }

    public boolean equals(Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof TraceData)) {
            return false;
        } else {
            TraceData other = (TraceData)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                label47: {
                    Object this$traceId = this.getTraceId();
                    Object other$traceId = other.getTraceId();
                    if (this$traceId == null) {
                        if (other$traceId == null) {
                            break label47;
                        }
                    } else if (this$traceId.equals(other$traceId)) {
                        break label47;
                    }

                    return false;
                }

                Object this$spanId = this.getSpanId();
                Object other$spanId = other.getSpanId();
                if (this$spanId == null) {
                    if (other$spanId != null) {
                        return false;
                    }
                } else if (!this$spanId.equals(other$spanId)) {
                    return false;
                }

                Object this$parentSpanId = this.getParentSpanId();
                Object other$parentSpanId = other.getParentSpanId();
                if (this$parentSpanId == null) {
                    if (other$parentSpanId != null) {
                        return false;
                    }
                } else if (!this$parentSpanId.equals(other$parentSpanId)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(Object other) {
        return other instanceof TraceData;
    }



    public TraceData() {
    }
}
