package com.fengkun.integration.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fengkun.integration.domain.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;


/**
 * @author fengkun
 */
@Mapper
public interface UserMapper extends BaseMapper<User> {

     User  getName(@Param("name") String name);

}
