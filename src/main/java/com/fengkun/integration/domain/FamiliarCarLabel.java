package com.fengkun.integration.domain;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

import java.util.List;

@Data
public class FamiliarCarLabel {

    @ExcelProperty(value = "司机手机号（必填）", index = 0)
    private String mobile;

    @ExcelProperty(value = "标签（选填）", index = 1)
    private String labels;

    /**
     * 失败原因
     */
    @ExcelProperty(value = "错误原因", index = 2)
    private String failedReason;

    @ExcelIgnore
    private List<String> labelList;

    @ExcelIgnore
    private Long userId;

    @ExcelIgnore
    private Integer userType;

    @ExcelIgnore
    private Integer index;
}
