/*
 * Copyright 2013-2018 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fengkun.integration.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.fengkun.integration.domain.User;
import com.fengkun.integration.dto.ResultDTO;
import com.fengkun.integration.mapper.UserMapper;
import com.fengkun.integration.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;


@RestController
@Api(tags = "用户管理")
@Slf4j
public class BasicController {

@Resource
private UserMapper userMapper;

    @Resource
    private UserService userService;

    @RequestMapping("/hello")
    @ApiOperation(value = "对接用户的管理")
     public ResultDTO<String> hello(@ApiParam(name = "用户id" ,required = true) @RequestParam  String name) {
        User user = userMapper.selectById(1);
        User fk = userMapper.getName("fk");
        User byId = userService.getUser(1);
        log.info("你好 java");
        return ResultDTO.success(null);
    }


    @RequestMapping("/hello1")
    @ApiOperation(value = "分页查询用户信息")
    public  IPage<User> getUserPage(Integer pagenum ,Integer pagesiz) {
        IPage<User> objectPage = new Page<>(pagenum, pagesiz);

        IPage<User> page = userService.page(objectPage, Wrappers.lambdaQuery());


        log.info("你好 java");
        return page ;
    }



}
