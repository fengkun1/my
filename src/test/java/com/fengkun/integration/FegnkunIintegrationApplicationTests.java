package com.fengkun.integration;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.ExcelReader;
import com.alibaba.excel.read.metadata.ReadSheet;
import com.fengkun.integration.common.util.OrderExcelDataListener;
import com.fengkun.integration.domain.FamiliarCarLabel;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import java.io.InputStream;

@SpringBootTest
class FegnkunIintegrationApplicationTests {

    @Test
    void contextLoads() {
        //解析本地数据
        String fileName = "demo.xlsx"; // Excel 文件路径

        try {
            // 打开 Excel 文件流
            // InputStream inputStream = new FileInputStream(fileName);
            //第一种方法
            //  InputStream resourceAsStream = this.getClass().getClassLoader().getResourceAsStream(fileName);
            //第二种方法
            Resource resource = new ClassPathResource(fileName);
            InputStream inputStream = resource.getInputStream();
            OrderExcelDataListener<FamiliarCarLabel> listener = new OrderExcelDataListener<>();

            EasyExcel.read(inputStream, FamiliarCarLabel.class, listener).sheet().headRowNumber(0).doRead();
            for (FamiliarCarLabel datum : listener.getData()) {
                System.out.println(datum);
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

}
